const { stubTrue } = require('lodash')

const env = require('./utils/env')()

const makeConfig = () => {
  return {
    debug: env('DEBUG', { optional: true, defaultValue: false }),
    stage: env('STAGE'),
    host: env('HOST', { optional: stubTrue }),
    db: {
      url: env('DB_URL', { optional: true })
    },
    http: {
      port: env('PORT', { optional: true }),
      token: env('HTTP_TOKEN', { optional: true })
    },
    redis: {
      url: env('SCALINGO_REDIS_URL', { optional: true })
    }
  }
}

// Use same logic as for setting config from env vars
// just to ensure that they are set.
const ensureVars = () => {
  env('TZ')
}

ensureVars()
const config = makeConfig()

if (env.hasMissingVars()) {
  const error = `[error] The following environment variables must be set:\n- ${env.getMissingVars().join('\n- ')}`

  if (process.env.NODE_ENV === 'test') {
    throw new Error(error)
  }

  console.error(error)
  process.exit(1)
}

module.exports = config
