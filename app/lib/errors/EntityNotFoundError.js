export default class EntityNotFoundError extends Error {
  constructor (entityName, entityId, message = '') {
    super(`Entity ${entityName} ${entityId ? `with ID ${entityId} ` : ''}not found${message && `; ${message}`}`)
    this.name = 'EntityNotFoundError'
  }
}
