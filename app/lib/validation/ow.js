import ow from 'ow'
import * as rules from './rules'

/**
 * @throws ArgumentError
 */
const checkObjectId = ow.create('objectId', ow.any(
  ow.object.is(rules.isObjectId),
  ow.string.is(rules.isObjectId)
))

/**
 * @throws ArgumentError
 */
const checkEmail = ow.create('email', ow.any(
  ow.string.is(rules.isEmail)
))

/**
 * @throws ArgumentError
 */
const checkSlug = ow.create('slug', ow.any(
  ow.string.is(rules.isSlug)
))

export {
  checkObjectId,
  checkEmail,
  checkSlug
}
