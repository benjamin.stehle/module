import mongoose from 'mongoose'

// eslint-disable-next-line
const emailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i
const hexColorRegex = /^#([0-9a-f]{3}|[0-9a-f]{6})$/i
export const slugRegex = /^[a-z0-9]+(-[a-z0-9]+)*$/

/**
 * Returns true also if it's a valid string representation of an ObjectId.
 */
function isObjectId (value) {
  return mongoose.Types.ObjectId.isValid(value)
}

const isSlug = value => slugRegex.test(String(value))
const isEmail = value => emailRegex.test(String(value))
const isHexColor = value => hexColorRegex.test(String(value))

export {
  isObjectId,
  isEmail,
  isHexColor,
  isSlug
}
