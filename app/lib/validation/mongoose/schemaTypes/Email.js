/**
 * Email schema borrowed from https://github.com/konsumer/mongoose-type-email/blob/master/index.js#L15
 * Since we have email validation at other places too, we need
 * to use our own email validation to be consistent.
 */

import mongoose from 'mongoose'
import { isEmail } from '../../rules'

function validateEmail (val, options) {
  var required = (typeof options.required === 'function') ? options.required() : options.required
  if (!required) {
    return true
  }
  return isEmail(val)
}

function Email (path, options) {
  this.options = options
  this.path = path
  mongoose.SchemaTypes.String.call(this, path, options)
  this.validate(
    val => validateEmail(val, options),
    options.message || Email.defaults.message || 'invalid email address'
  )
}

Email.defaults = {}

Object.setPrototypeOf(Email.prototype, mongoose.SchemaTypes.String.prototype)

Email.prototype.cast = function (val) {
  return val.trim().toLowerCase()
}

Email.prototype.get = function (val) {
  return val.trim().toLowerCase()
}

Email.prototype.checkRequired = function (val) {
  return typeof val === 'string' && validateEmail(val, this.options)
}

mongoose.SchemaTypes.Email = Email
mongoose.Types.Email = String

export default Email
