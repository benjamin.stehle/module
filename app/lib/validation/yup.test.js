const yup = require('yup')
const { addDefaultMethods } = require('./yup')

// IMPORTANT: Don't remove these lines (mock events & FeatureRegistry) unless you really know what you do!
jest.mock('@/app/lib/events')
jest.mock('@/app/events')
jest.mock('@/app/modules/feature/features/FeatureRegistry')

addDefaultMethods()

describe('validation', () => {
  describe('yup', () => {
    describe('phoneNumber', () => {
      const schema = yup.string().phoneNumber()

      describe('transformation', () => {
        it('passes on +4915209896931 (valid phone number with leading +)', async () => {
          const validated = await schema.validate('+4915209896931')
          expect(validated).toBe('+4915209896931')
        })

        it('transforms 004915209896931 to start with a +', async () => {
          const validated = await schema.validate('004915209896931')
          expect(validated).toBe('+4915209896931')
        })

        it('transforms +49015209896931 to valid phone number', async () => {
          const validated = await schema.validate('+49015209896931')
          expect(validated).toBe('+4915209896931')
        })

        it('transforms 0049015209896931 to valid phone number', async () => {
          const validated = await schema.validate('0049015209896931')
          expect(validated).toBe('+4915209896931')
        })

        it('transforms +49 152 09896931 to valid phone number', async () => {
          const validated = await schema.validate('+49 152 09896931')
          expect(validated).toBe('+4915209896931')
        })

        it('transforms +49 (152) 09896931 to valid phone number', async () => {
          const validated = await schema.validate('+49 (152) 09896931')
          expect(validated).toBe('+4915209896931')
        })

        it('throws on 015209896931 (missing country calling code)', async () => {
          expect.assertions(2)

          try {
            await schema.validate('015209896931')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 15209896931 (missing country calling code)', async () => {
          expect.assertions(2)

          try {
            await schema.validate('15209896931')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 11111', async () => {
          expect.assertions(2)

          try {
            await schema.validate('11111')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 12345', async () => {
          expect.assertions(2)

          try {
            await schema.validate('12345')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +49123456789', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+49123456789')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 4915209896931', async () => {
          expect.assertions(2)

          try {
            await schema.validate('4915209896931')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +4915211111131', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+4915211111131')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +4915212345631', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+4915212345631')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +490000', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+49 0000')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })
      })

      describe('validate context { tolerant: true }', () => {
        const options = {
          context: {
            phoneNumber: {
              tolerant: true
            }
          }
        }

        it('unsets invalid number', async () => {
          const validated = await schema.validate('11111', options)
          expect(validated).toBeUndefined()
        })

        it('keeps valid number', async () => {
          const validated = await schema.validate('+4915209896931', options)
          expect(validated).toBe('+4915209896931')
        })

        it('formats possible number', async () => {
          const validated = await schema.validate('+491312322234', options)
          expect(validated).toBe('+491312322234')
        })
      })

      describe('validate context { defaultCountryCode: "de" }', () => {
        const getOptions = defaultCountryCode => ({
          context: {
            phoneNumber: {
              defaultCountryCode
            }
          }
        })

        it('considers defaultCountryCode for number without country calling code', async () => {
          const validated = await schema.validate('015209896931', getOptions('de'))
          expect(validated).toBe('+4915209896931')
        })

        it('throws if number does not match the defaultCountryCode', async () => {
          expect.assertions(2)

          try {
            await schema.validate('015209896931', getOptions('ch'))
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })
      })

      // Basically the same tests as in "transformation" tests but
      // without transformation (as this must not work here)
      describe('validate options { strict: true }', () => {
        const options = { strict: true }

        it('passes on +4915209896931 (valid phone number with leading +)', async () => {
          const validated = await schema.validate('+4915209896931', options)
          expect(validated).toBe('+4915209896931')
        })

        it('throws on 015209896931 (missing country calling code)', async () => {
          expect.assertions(2)

          try {
            await schema.validate('015209896931', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 15209896931 (missing country calling code)', async () => {
          expect.assertions(2)

          try {
            await schema.validate('15209896931', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 11111', async () => {
          expect.assertions(2)

          try {
            await schema.validate('11111', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 12345', async () => {
          expect.assertions(2)

          try {
            await schema.validate('12345', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +49123456789', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+49123456789', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on 4915209896931', async () => {
          expect.assertions(2)

          try {
            await schema.validate('4915209896931', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +4915211111131', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+4915211111131', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        it('throws on +4915212345631', async () => {
          expect.assertions(2)

          try {
            await schema.validate('+4915212345631', options)
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })

        // Special test to be sure transformation does not come into place
        it('passes but does not transform to proper format', async () => {
          const validated = await schema.validate('+49015209896931', options)
          expect(validated).toBe('+49015209896931')
        })
      })

      describe('embedded in parent schema', () => {
        it('passes', async () => {
          const wrapperSchema = yup.object().shape({
            phone: schema
          })

          const validated = await wrapperSchema.validate({
            phone: '+4915209896931'
          })

          expect(validated).toEqual({ phone: '+4915209896931' })
        })

        // That's an important test because normally child schemas are
        // considered strict which would cause the test function will be
        // executed as well in a separate run.
        it('passes even one level deeper with only transformation having an effect (possible number)', async () => {
          const wrapperSchema = yup.object().shape({
            personalData: yup.object().shape({
              phone: schema
            })
          })

          const validated = await wrapperSchema.validate({
            personalData: {
              phone: '+491312322234'
            }
          }, {
            context: {
              phoneNumber: {
                tolerant: true
              }
            }
          })

          expect(validated).toEqual({
            personalData: {
              phone: '+491312322234'
            }
          })
        })
      })

      // Here we just test a few cases since it should be the same behavior as
      // for validate options { strict: true }
      describe('.strict()', () => {
        const strictSchema = schema.strict()

        it('passes for valid number', async () => {
          const validated = await strictSchema.validate('+4915209896931')
          expect(validated).toBe('+4915209896931')
        })

        it('passes but does not transform to proper format', async () => {
          const validated = await strictSchema.validate('+49015209896931')
          expect(validated).toBe('+49015209896931')
        })

        it('throws on invalid number', async () => {
          expect.assertions(2)

          try {
            await strictSchema.validate('+4915212345631')
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
            expect(err.message).toBe('INVALID_PHONE_ERROR')
          }
        })
      })

      describe('.optional()', () => {
        const wrapperSchema = yup.object().shape({
          phone: schema.optional()
        })

        it('passes', async () => {
          const validated = await wrapperSchema.validate({})
          expect(validated).toEqual({})
        })
      })

      describe('.nullable()', () => {
        const wrapperSchema = yup.object().shape({
          phone: schema.nullable()
        })

        it('passes', async () => {
          const validated = await wrapperSchema.validate({
            phone: null
          })
          expect(validated).toEqual({ phone: null })
        })
      })

      describe('.optional().nullable()', () => {
        const wrapperSchema = yup.object().shape({
          phone: schema.optional().nullable()
        })

        it('passes', async () => {
          const validated = await wrapperSchema.validate({})
          expect(validated).toEqual({})
        })
      })

      describe('.required()', () => {
        const wrapperSchema = yup.object().shape({
          phone: schema.required()
        })

        it('passes', async () => {
          const validated = await wrapperSchema.validate({
            phone: '+4915209896931'
          })
          expect(validated).toEqual({ phone: '+4915209896931' })
        })

        it('failes on null', async () => {
          expect.assertions(1)

          try {
            await await wrapperSchema.validate({
              phone: null
            })
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
          }
        })

        it('failes if not present', async () => {
          expect.assertions(1)

          try {
            await await wrapperSchema.validate({})
          } catch (err) {
            expect(err).toBeInstanceOf(yup.ValidationError)
          }
        })
      })
    })
  })
})
