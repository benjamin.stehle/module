import * as yup from 'yup'
import { isEmail, isObjectId } from './rules'

function addDefaultMethods () {
  yup.addMethod(yup.string, 'objectId', function () {
    return this.test(
      'is-objectId',
      // eslint-disable-next-line
      '${path} is not a valid id',
      function (value) {
        return isObjectId(value)
      })
  })

  yup.addMethod(yup.string, 'email', function () {
    return this.test(
      'email',
      // eslint-disable-next-line
      '${path} is not a valid email address',
      function (value) {
        if (!value) {
          return true
        }
        return isEmail(value)
      })
  })

  yup.addMethod(yup.mixed, 'isInOnboarding', function () {
    return this.when('$isInOnboarding', {
      is: true,
      then: this.nullable(),
      else: this.required()
    })
  })

  // Make "oneOf" rule optional. @see https://github.com/jquense/yup/issues/104#issuecomment-659508687
  yup.addMethod(yup.string, 'oneOfOptional', (arr, message) => {
    return yup.mixed().test({
      message,
      name: 'oneOfOptional',
      exclusive: true,
      params: {},
      test (value) {
        return !value ? true : arr.includes(value)
      }
    })
  })
}

export {
  addDefaultMethods
}
