import { addDefaultMethods } from './yup'
export * from './ow'

function setupValidation () {
  addDefaultMethods()
}

export {
  setupValidation
}
