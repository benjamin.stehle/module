export default {
  LEAD_CREATED: 'Lead Created',
  LEAD_UPDATED: 'Lead Updated',
  EMAIL_SENT: 'EMail Sent'
}
