import EventEmitter from 'events'
import logger from '../logger'
import InvalidArgumentError from '../errors/InvalidArgumentError'
import names from './names'

const eventLogger = logger.child({ type: 'event' })

class Emitter extends EventEmitter {
  emit (type, ...args) {
    eventLogger.info(type)
    super.emit(type, ...args)
  }

  on (name, ...args) {
    if (!name) {
      throw new InvalidArgumentError('Invalid event name passed to event handler')
    }
    super.on(name, ...args)
  }

  get names () {
    return names
  }
}

const ee = new Emitter()

ee.on('error', (err) => {
  logger.error(err, { eventHandler: true })
})

export default ee
