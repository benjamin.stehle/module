import Redis from 'ioredis'
import config from '../config'
import logger from './logger'

function createRedisClient () {
  const client = new Redis(config.redis.url, {
    reconnectOnError, // Probably not needed
    maxRetriesPerRequest: null,
    connectTimeout: 30000,
    enableReadyCheck: false
  })

  if (process.env.NODE_ENV !== 'test') {
    attachListeners(client)
  }

  return client
}

function reconnectOnError (err) {
  console.error('=== redis reconnect')
  console.error(err)

  const targetError = 'ETIMEDOUT'
  if (err.message.includes(targetError)) {
    return 2 // 2=reconnect and resend 1=just reconnect
  }
}

function attachListeners (client) {
  client
    .on('error', err => {
      console.log('=== redis error (logged to logging service)')
      logger.error(err)
    })

  if (config.debug) {
    client
      .on('connect', () => {
        console.log('=== redis connect')
      })
      .on('ready', () => {
        console.log('=== redis ready')
      })
      .on('close', () => {
        console.log('=== redis close')
      })
      .on('reconnecting', () => {
        console.log('=== redis reconnecting')
      })
      .on('end', () => {
        console.log('=== redis end')
      })
  }
}

export {
  createRedisClient
}
