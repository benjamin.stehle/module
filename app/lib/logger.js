import winston, { format } from 'winston'
import SentryTransport from 'winston-transport-sentry-node'
import { consoleFormat } from 'winston-console-format'

const sentryFormat = format((info) => {
  const { module, tags = {}, ...extra } = info
  return {
    ...extra,
    tags: {
      ...tags,
      module
    }
  }
})

const ignoreEventLogs = format((log) => log.type === 'event' ? false : log)

const logger = winston.createLogger({
  transports: [
    new SentryTransport({
      level: 'warn',
      format: sentryFormat(),
      skipSentryInit: true
    })
  ]
})

if (process.env.NODE_ENV === 'development') {
  logger.add(
    new winston.transports.Console({
      level: 'debug',
      format: winston.format.combine(
        ignoreEventLogs(),
        format.timestamp(),
        format.ms(),
        format.errors({ stack: true }),
        format.splat(),
        format.json(),
        format.colorize({ all: true }),
        format.padLevels(),
        consoleFormat({
          showMeta: true,
          metaStrip: ['timestamp'],
          inspectOptions: {
            depth: Infinity,
            colors: true,
            maxArrayLength: Infinity,
            breakLength: 120,
            compact: Infinity
          }
        })
      )
    })
  )
}

export default logger
