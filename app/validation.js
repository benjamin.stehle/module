const { UserInputError } = require('apollo-server-express')
const yup = require('yup')
const _ = require('lodash')

module.exports = ({ services }) => {
  yup.addMethod(yup.string, 'isAvailableOnNetlify', function () {
    return this.test(
      'is-available-on-netlify',
      // eslint-disable-next-line no-template-curly-in-string
      '${path} is not available on netlify',
      async function (value) {
        return services.netlify.slugDoesNotExists(value)
      }
    )
  })

  yup.addMethod(yup.mixed, 'file', function () {
    return this.test(
      'is-file',
      // eslint-disable-next-line no-template-curly-in-string
      '${path} is not a file',
      async function (value) {
        return !_.has(await value, ['createReadStream', 'mimetype'])
      }
    )
  })

  const validate = async (data, schema, context) => {
    try {
      await schema.validate(data, {
        abortEarly: false,
        strict: true,
        context
      })
    } catch (error) {
      const details = error.inner.map(({ path, type, message }) => {
        return {
          path,
          type,
          message
        }
      })

      throw new UserInputError('Validation failed.', {
        details
      })
    }
  }

  return validate
}
