import express from 'express'

const router = express.Router()
router.use(require('./lead'))

export default router
