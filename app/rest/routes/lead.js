import express from 'express'
import ee from '../../lib/events'
import leadApi from '../../modules/lead/publicApi'

const router = express.Router()

router.post('/events', (req, res) => {
  const { sender, event, data } = req.body
  console.log({ sender, event, data })
  ee.emit(`${sender}:${event}`, data)
  res.sendStatus(200)
})

router.post('/lead', async ({ body }, res) => {
  console.log(body)
  try {
    const lead = await leadApi.createLead(body)
    res.json(lead)
  } catch (error) {
    console.log(error)
    res.sendStatus(500)
  }
})

module.exports = router
