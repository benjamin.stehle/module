import * as yup from 'yup'

export default yup.object().shape({
  email: yup.string().lowercase().trim().email().required(),
  firstname: yup.string().required(),
  lastname: yup.string().required(),
  address: yup.object().shape({
    zip: yup.string().min(4).max(5).nullable(),
    street: yup.string().nullable(),
    houseNumber: yup.string().nullable(),
    city: yup.string().nullable(),
    countryCode: yup.string().nullable()
  }).required()
})
