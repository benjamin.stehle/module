import { createLead } from '../services/leadService'
import createLeadSchema from './createLeadSchema'

const api = {
  async createLead (input) {
    const validatedInput = await createLeadSchema.validate(input, { stripUnknown: true })
    return createLead(validatedInput)
  }
}

export default api
