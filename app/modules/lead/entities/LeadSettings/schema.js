import { Schema } from 'mongoose'
import PostalShipping from './enums/PostalShipping'
import statics from './statics'

const leadSettingsSchema = new Schema(
  {
    company: { type: Schema.Types.ObjectId, ref: 'Company' },
    postalShipping: {
      type: {
        type: String,
        enum: Object.values(PostalShipping),
        default: PostalShipping.NONE
      },
      allGuides: { type: Boolean, default: true },
      activeLandingpages: { type: [String], default: [] }
    }
  },
  {
    timestamps: true,
    toObject: {
      virtuals: true
    }
  }
)

leadSettingsSchema.plugin(statics)

leadSettingsSchema.index({ company: 1 }, { unique: true })

export default leadSettingsSchema
