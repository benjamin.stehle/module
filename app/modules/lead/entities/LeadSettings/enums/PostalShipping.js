export const PostalShipping = Object.freeze({
  NONE: 'NONE',
  AUTOMATIC: 'AUTOMATIC',
  MANUAL: 'MANUAL'
})

export default PostalShipping
