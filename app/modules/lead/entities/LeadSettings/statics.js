
import { checkObjectId } from '../../../../lib/validation/ow'

const statics = {
  findByCompanyId (companyId) {
    checkObjectId(companyId)
    return this.findOne({ company: companyId })
  }
}

export default function (schema) {
  Object.assign(schema.statics, statics)
}
