import { model } from 'mongoose'
import schema from './schema'

const MODEL_NAME = 'LeadSettings'
const COLLECTION_NAME = 'lead.settings'

export default model(MODEL_NAME, schema, COLLECTION_NAME)
