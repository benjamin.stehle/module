import { model } from 'mongoose'
import schema from './schema'

const MODEL_NAME = 'Lead'
const COLLECTION_NAME = 'lead.leads'

export default model(MODEL_NAME, schema, COLLECTION_NAME)
