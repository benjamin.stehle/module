const Gender = Object.freeze({
  MALE: 'MALE',
  FEMALE: 'FEMALE',
  DIVERS: 'DIVERS'
})

export default Gender
