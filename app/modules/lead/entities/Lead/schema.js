import { Schema } from 'mongoose'
import Email from '../../../../lib/validation/mongoose/schemaTypes/Email'

export const addressSchema = new Schema({
  countryCode: {
    type: String,
    required: true
  },
  zip: {
    type: String,
    trim: true,
    required: true
  },
  city: {
    type: String,
    trim: true,
    required: true
  },
  street: {
    type: String,
    trim: true,
    required: true
  },
  houseNumber: {
    type: String,
    trim: true
  }
}, {
  _id: false
})

const leadSchema = new Schema({
  email: {
    type: Email,
    required () {
      return !this.deletedAt
    }
  },
  address: {
    type: addressSchema,
    required: false
  },
  firstname: {
    type: String,
    trim: true,
    default: ''
  },
  lastname: {
    type: String,
    trim: true,
    default: ''
  },
  gender: {
    type: String,
    default: ''
  }
}, {
  timestamps: true,
  toObject: {
    virtuals: true
  }
})

leadSchema.virtual('fullName').get(function () {
  return [this.firstname, this.lastname]
    .filter(Boolean)
    .join(' ')
})

leadSchema.index({ email: 1 }, { unique: true })

export default leadSchema
