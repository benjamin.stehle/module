import logger from '../../../lib/logger'

const leadLogger = logger.child({ module: 'lead' })

export default leadLogger
