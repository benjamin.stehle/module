import { getLead } from './services/leadService'

let initialized = false

function initializeModule () {
  if (!initialized) {
    initialized = true
  }
}

export {
  initializeModule,
  getLead
}
