import ee from '../../../lib/events'
import Lead from '../entities/Lead'

async function createLead (input) {
  const lead = await Lead.create(input)
  ee.emit(ee.names.LEAD_CREATED, { leadId: lead._id })
  return lead
}

async function getLead (leadId) {
  return Lead.findById(leadId)
}

export {
  createLead,
  getLead
}
