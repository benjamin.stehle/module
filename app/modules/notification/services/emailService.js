import ee from '../../../lib/events'
import { getLead } from '../../lead'

async function sendMail (leadId) {
  const lead = await getLead(leadId)
  console.log('SEND MAIL', lead)
  // throw new Error('Failed')
  ee.emit(ee.names.EMAIL_SENT, { leadId })
  return lead
}

export {
  sendMail
}
