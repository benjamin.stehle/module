import logger from '../../../lib/logger'

const leadLogger = logger.child({ module: 'notification' })

export default leadLogger
