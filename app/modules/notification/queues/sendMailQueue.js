import { Queue, QueueScheduler, Worker } from 'bullmq'
import { createRedisClient } from '../../../lib/redis'
import leadLogger from '../../lead/lib/leadLogger'
import { sendMail } from '../services/emailService'

const QUEUE_NAME = 'notification.sendMail'
const ATTEMPTS = 5

let queue = null

function createScheduler () {
  return new QueueScheduler(QUEUE_NAME, { connection: createRedisClient() })
}

const sendMailQueue = (function () {
  if (!queue) {
    queue = createQueue()
    createWorker()
    createScheduler()
  }
  return queue
})()

function createQueue () {
  return new Queue(QUEUE_NAME, {
    connection: createRedisClient(),
    defaultJobOptions: {
      attempts: ATTEMPTS,
      backoff: {
        type: 'exponential',
        delay: 3000
      }
    }
  })
}

function createWorker () {
  const worker = new Worker(QUEUE_NAME, processor, {
    connection: createRedisClient()
  })

  worker.on('failed', (job, reason) => {
    if (job.attemptsMade >= ATTEMPTS) {
      leadLogger.error(reason, {
        queue: QUEUE_NAME,
        jobName: job.name,
        jobId: job.id
      })
    }
  })

  return worker
}

async function processor (job) {
  return sendMail(job.data.lead)
}

export default sendMailQueue
