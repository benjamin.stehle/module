import leadCreated from './leadCreated'

function registerHandlers () {
  leadCreated.registerHandler()
}

export default registerHandlers
