import ee from '../../../lib/events'
import notificationLogger from '../lib/notificationLogger'
import sendMailQueue from '../queues/sendMailQueue'

function registerHandler () {
  ee.on(ee.names.LEAD_CREATED, (...args) => {
    setImmediate(() => {
      handleEvent(...args).catch(err => {
        notificationLogger.error(err)
      })
    })
  })
}

async function handleEvent ({ leadId }) {
  sendMailQueue.add(ee.names.LEAD_CREATED, { leadId })
}

export default {
  registerHandler
}
