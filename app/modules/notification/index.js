import registerHandlers from './handlers'

let initialized = false

function initializeModule () {
  if (!initialized) {
    registerHandlers()
    initialized = true
  }
}

export {
  initializeModule
}
