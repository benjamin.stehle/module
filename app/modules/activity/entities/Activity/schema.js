import statics from './statics'
import Type from './enums/Type'
const { Schema } = require('mongoose')

const activitySchema = new Schema({
  type: {
    type: String,
    enum: Object.values(Type),
    required: true
  },
  leadId: {
    type: Schema.Types.ObjectId,
    ref: 'Lead',
    required: true
  }
}, {
  timestamps: true
})

activitySchema.plugin(statics)

activitySchema.index({ lead: 1 })

export default activitySchema
