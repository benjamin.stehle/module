import { model } from 'mongoose'
import schema from './schema'

const MODEL_NAME = 'Activity'
const COLLECTION_NAME = 'lead.activities'

export default model(MODEL_NAME, schema, COLLECTION_NAME)
