import { checkObjectId } from '../../../../lib/validation'

const statics = {
  findByLeadId (leadId) {
    checkObjectId(leadId)
    return this.find({ lead: leadId })
  }
}

export default function (schema, options) {
  Object.assign(schema.statics, statics)
}
