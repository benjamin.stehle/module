import logger from '../../../lib/logger'

const leadLogger = logger.child({ module: 'activity' })

export default leadLogger
