import Activity from '../entities/Activity'

async function createMailSentActivity ({ leadId, type }) {
  console.log({ leadId, type })
  return Activity.create({ leadId, type })
}

export {
  createMailSentActivity
}
