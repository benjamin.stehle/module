import ee from '../../../lib/events'
import Type from '../entities/Activity/enums/Type'
import activityLogger from '../lib/activityLogger'
import { createMailSentActivity } from '../services/activityService'

function registerHandler () {
  ee.on(ee.names.EMAIL_SENT, (...args) => {
    setImmediate(() => {
      handleEvent(...args).catch(err => {
        activityLogger.error(err)
      })
    })
  })
}

async function handleEvent ({ leadId }) {
  createMailSentActivity({ leadId, type: Type.EMAIL_SENT })
}

export default {
  registerHandler
}
