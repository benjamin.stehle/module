import mailSent from './mailSent'

function registerHandlers () {
  mailSent.registerHandler()
}

export default registerHandlers
