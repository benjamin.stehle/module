import * as notification from './modules/notification'
import * as lead from './modules/lead'
import * as activity from './modules/activity'

function registerModules () {
  notification.initializeModule()
  lead.initializeModule()
  activity.initializeModule()
}

export default registerModules
