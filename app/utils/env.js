function env () {
  const missingVars = []

  function get (name, options = {}) {
    const value = process.env[name]
    const {
      // If string or [string] it's considered as environment name(s)
      // meaning it's only optional in the specified environments.
      // If boolean it's considered optional for every environment.
      optional = false,
      defaultValue = undefined,
      parser = value => value
    } = options

    if (!isUndefined(value)) {
      return parser(value)
    }

    if (optional === true) {
      return defaultValue
    }

    if (!isBoolean(optional)) {
      const optionalEnvironments = ensureArray(optional)

      if (optionalEnvironments.indexOf(process.env.NODE_ENV) !== -1) {
        return defaultValue
      }
    }

    missingVars.push(name)
  }

  get.hasMissingVars = () => missingVars.length > 0
  get.getMissingVars = () => missingVars

  return get
}

const ensureArray = value => {
  if (Array.isArray(value)) {
    return value
  }
  return [value]
}

const isUndefined = value => typeof value === 'undefined'
const isBoolean = value => typeof value === typeof true

module.exports = env
