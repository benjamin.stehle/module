import { setupValidation } from './lib/validation'
import registerModules from './registerModules'
var httpContext = require('express-http-context')
const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

setupValidation()

app.use(cors())
app.use(bodyParser.json({ limit: '8mb' }))
app.use(cookieParser())

registerModules()

app.use(httpContext.middleware)

const routes = require('./rest/routes').default
app.use(routes)

app.get('/_health', (req, res) => {
  res.json({
    status: 'ok'
  })
})

module.exports = {
  app
}
