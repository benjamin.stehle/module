const mongoose = require('mongoose')

const connect = url => {
  return mongoose.connect(url, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
}

const disconnect = () => {
  return mongoose.disconnect()
}

module.exports = {
  connect,
  disconnect
}
