const http = require('http')
const { createTerminus } = require('@godaddy/terminus')

const config = require('./app/config')
const { app } = require('./app')
const db = require('./db')

const server = http.createServer(app)

const options = {
  onSignal: () => Promise.all([db.disconnect()])
}

createTerminus(server, options)

const start = async () => {
  console.log(config.db.url);
  await db.connect(config.db.url)

  server.listen(config.http.port, () => {
    console.log(`Service started on http://localhost:${config.http.port}`)
  })
}

start()
